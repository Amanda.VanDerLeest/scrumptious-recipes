from django.urls import path

from meal_plan.views import (
    MealPlanListView,  # class and import added to views with weird code to check on
    MealPlanDetailView,  # class and import added to views with pass
    MealPlanDeleteView,  # class and import added to views with the recommended "list" information?
    MealPlanCreateView,  # class and impoer added to views with pass
    MealPlanUpdateView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plan"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
    path("new/", MealPlanCreateView.as_view(), name="meal_plan_new"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plan_edit"),
]
