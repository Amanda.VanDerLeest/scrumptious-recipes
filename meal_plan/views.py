from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plan.models import Meal_Plan

# from meal_plans.forms import MealPlanForm, MealPlanDeleteForm


class MealPlanListView(LoginRequiredMixin, ListView):
    model = Meal_Plan
    template_name = "meal_plan/list.html"
    paginate_by = 10

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)


class MealPlanDetailView(DetailView):
    model = Meal_Plan
    template_name = "meal_plan/detail.html"

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plan
    template_name = "meal_plan/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect(
            "meal_plan_detail", pk=plan.id
        )  # meal_plan_detail is redirecting it to the detail page


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_Plan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("meal_plan")

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_Plan
    template_name = "meal_plan/edit.html"
    fields = ["name", "date", "recipes"]

    def get_queryset(self):
        return Meal_Plan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy(
            "meal_plan_detail", args=[self.object.id]
        )  # meal_plan_detail is redirecting it to the detail page
