from django.contrib import admin

from meal_plan.models import Meal_Plan

# Register your models here.
admin.site.register(Meal_Plan)
