from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class Meal_Plan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(auto_now_add=False)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="recipes_owned",
        on_delete=models.CASCADE,
        null=True,
    )

    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")

    def __str__(self):
        return self.name + " by " + str(self.author)
