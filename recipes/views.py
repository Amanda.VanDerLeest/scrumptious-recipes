from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError

from recipes.forms import RatingForm


from recipes.models import Recipe, Ingredient, Shopping_Item


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)

            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")

            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


def create_shopping_item(request):
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method
    ingredient_id = request.POST.get("ingredient_id")

    # POST METHOD IS USED WHEN YOU WANT TO SEND SOME DATA TO THE SERVER-
    # IN THIS CASE, WE WANT TO SEND THE INGREDIENT TO THE SHOPPING LIST

    # Get the specific ingredient from the Ingredient model
    # using the code
    # Ingredient.objects.get(id=the value from the dictionary)
    ingredient = Ingredient.objects.get(id=ingredient_id)

    # Get the current user which is stored in request.user
    current_user = request.user

    try:
        Shopping_Item.objects.create(
            food_item=ingredient.food, user=current_user
        )

    except IntegrityError:  # Raised if someone tries to add
        pass  # the same food twice, just ignore it

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    # return redirect(
    #     name of the registered recipe detail path,
    #     pk=id of the ingredient's recipe
    # )
    return redirect("recipe_detail", pk=ingredient.recipe.id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy(
        "recipes_list"
    )  # This brings you back to the listview page that has the list of recipes after successfully creating a recipe. If you change it to recipe_new, it'll just bring you back to make a new recipe page

    # this is where you set the author to the person logged in
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # line 80 says we're creating a copy of create view (inheriting) and making these tweeks to it.
    # Line 88 has super class, which comes from createview because of inheritance. Parent class & child class. Superclass and subclass
    # ln 88 says I want to make this specific change when we have a valid form
    # line 89 says okay, createview, do the form valid (whatever you want to do when you have a valid form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
